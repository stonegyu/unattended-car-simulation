﻿using UnityEngine;
using System.Collections;
public struct NodeInfo{
	public GameObject obj;
	public Vector3 Position;
	public float CumulativeDistance;
	public bool visited;
	public bool endPosition;
	public int x;
	public int y;
};
public class RoadSearchNode : MonoBehaviour {
	Queue NodeQueue = new Queue();
	public Material ArriveMaterial;
	public Material NormalMaterial;
	int[] NodeWeight;


	NodeInfo[,] Node = new NodeInfo[5,5];
	void BFS_SearchNode(){
		NodeInfo LocNode = (NodeInfo)NodeQueue.Dequeue ();
		if(Node[LocNode.y,LocNode.x].visited == true) {
			if(NodeQueue.Count > 0) BFS_SearchNode();
			return;
		}
		Node [LocNode.y, LocNode.x].visited = true;

		if(LocNode.endPosition == true){
			return;
		}
		if(LocNode.x-1 >= 0 && Node[LocNode.y,LocNode.x-1].visited == false){
			Node[LocNode.y,LocNode.x-1].CumulativeDistance = LocNode.CumulativeDistance + Vector3.Distance(LocNode.Position, Node[LocNode.y,LocNode.x-1].Position);
			NodeQueue.Enqueue(Node[LocNode.y,LocNode.x-1]);
		}
		if(LocNode.x+1 < 5 && Node[LocNode.y,LocNode.x+1].visited == false){
			Node[LocNode.y,LocNode.x+1].CumulativeDistance = LocNode.CumulativeDistance + Vector3.Distance(LocNode.Position, Node[LocNode.y,LocNode.x+1].Position);
			NodeQueue.Enqueue(Node[LocNode.y,LocNode.x+1]);
		}
		if(LocNode.y-1 >= 0 && Node[LocNode.y-1,LocNode.x].visited == false){
			Node[LocNode.y-1,LocNode.x].CumulativeDistance = LocNode.CumulativeDistance + Vector3.Distance(LocNode.Position, Node[LocNode.y-1,LocNode.x].Position);
			NodeQueue.Enqueue(Node[LocNode.y-1,LocNode.x]);
		}
		if(LocNode.y+1 < 5 && Node[LocNode.y+1,LocNode.x].visited == false){
			Node[LocNode.y+1,LocNode.x].CumulativeDistance = LocNode.CumulativeDistance + Vector3.Distance(LocNode.Position, Node[LocNode.y+1,LocNode.x].Position);
			NodeQueue.Enqueue(Node[LocNode.y+1,LocNode.x]);
		}
		BFS_SearchNode ();
	}

	public Queue NearestNodeQueue = new Queue();
	void Nearest_Road(int x, int y, int minXX, int maxXX, int minYY, int maxYY, int endX, int endY){
		NearestNodeQueue.Enqueue (Node [y, x]);
		if (x == endX && y == endY) return;
		print ("" + x + " " + y + " " + minXX + " " + maxXX + " " + maxYY + " " + endX + " " + endY);
		float min = 1000000;
		int minX = -1;
		int minY = -1;
		if(x-1 >=minXX){
			if(x-1 == endX && y == endY){
				Nearest_Road(x-1, y, minXX, maxXX, minYY, maxYY , endX, endY);
				return;
			}
			if(Node[y,x].CumulativeDistance < Node[y,x-1].CumulativeDistance){
				if(min > Node[y,x-1].CumulativeDistance){
					min = Node[y,x-1].CumulativeDistance;
					minX = x-1;
					minY = y;
				}

			}
		}
		if(x+1 <= maxXX){
			if(x+1 == endX && y == endY){
				Nearest_Road(x+1, y, minXX, maxXX, minYY, maxYY, endX, endY);
				return;
			}
			if(Node[y,x].CumulativeDistance < Node[y,x+1].CumulativeDistance){
				if(min > Node[y,x+1].CumulativeDistance){
					min = Node[y,x+1].CumulativeDistance;
					minX = x+1;
					minY = y;
				}
			}
		}
		if(y-1 >=minYY){
			if(x == endX && y-1 == endY){
				Nearest_Road(x, y-1, minXX, maxXX, minYY, maxYY,endX, endY);
				return;
			}
			if(Node[y,x].CumulativeDistance < Node[y-1,x].CumulativeDistance){
				if(min > Node[y-1,x].CumulativeDistance){
					min = Node[y-1,x].CumulativeDistance;
					minX = x;
					minY = y-1;
				}
			}
		}
		if(y+1 <= maxYY){
			if(x == endX && y+1 == endY){
				Nearest_Road(x, y+1, minXX, maxXX, minYY, maxYY,endX, endY);
				return;
			}
			if(Node[y,x].CumulativeDistance < Node[y+1,x].CumulativeDistance){
				if(min > Node[y+1,x].CumulativeDistance){
					min = Node[y+1,x].CumulativeDistance;
					minX = x;
					minY = y+1;
				}
			}
		}
		if(minX != -1 && minY != -1) {
			Nearest_Road (minX, minY, minXX, maxXX, minYY, maxYY,endX, endY);
		}
	}
	public void init(){
		int j = 0;
		for(int i = 0; i < 25; i++){
			j = i/5;
			Node[j,i%5].obj = GameObject.Find(""+i).gameObject;
			Node[j,i%5].Position = GameObject.Find(""+i).transform.position;
			Node[j,i%5].CumulativeDistance = 0;
			Node[j,i%5].visited = false;
			Node[j,i%5].endPosition = false;
			Node[j,i%5].x = i%5;
			Node[j,i%5].y = j;

			Node[j,i%5].obj.renderer.material = NormalMaterial;
		}

		int startPosX = Random.Range (0,4), startPosY = Random.Range (0,4);
		int endPosX = (startPosX+Random.Range (1,4))%5, endPosY = (startPosY+Random.Range(1,4))%5;
		setEndPosition (endPosX, endPosY);
		setStartPosition (startPosX, startPosY, endPosX, endPosY);

		int cnt = NearestNodeQueue.Count;
		for(int i = 0; i < cnt; i++){
			NodeInfo temp = (NodeInfo)NearestNodeQueue.Dequeue();
			temp.obj.renderer.material = ArriveMaterial;
			NearestNodeQueue.Enqueue(temp);
		}
	}
	public void reLoad(int curPosX, int curPosY){
		print ("curX " + curPosX +" curY" + curPosY);
		int j = 0;
		for(int i = 0; i < 25; i++){
			j = i/5;
			Node[j,i%5].obj = GameObject.Find(""+i).gameObject;
			Node[j,i%5].Position = GameObject.Find(""+i).transform.position;
			Node[j,i%5].CumulativeDistance = 0;
			Node[j,i%5].visited = false;
			Node[j,i%5].endPosition = false;
			Node[j,i%5].x = i%5;
			Node[j,i%5].y = j;

			Node[j,i%5].obj.renderer.material = NormalMaterial;
		}

		int startPosX = curPosX, startPosY = curPosY;
		int endPosX = (startPosX+Random.Range (1,4))%5, endPosY = (startPosY+Random.Range(1,4))%5;
		setEndPosition (endPosX, endPosY);
		setStartPosition (startPosX, startPosY, endPosX, endPosY);

		int cnt = NearestNodeQueue.Count;
		for(int i = 0; i < cnt; i++){
			NodeInfo temp = (NodeInfo)NearestNodeQueue.Dequeue();
			temp.obj.renderer.material = ArriveMaterial;
			NearestNodeQueue.Enqueue(temp);
		}
	}
	void setStartPosition(int startX, int startY, int endX, int endY){

		int minXX = (startX < endX) ? startX : endX;
		int maxXX = (startX > endX) ? startX : endX;
		int minYY = (startY < endY) ? startY : endY;
		int maxYY = (startY > endY) ? startY : endY;

		NodeQueue.Enqueue (Node [startY,startX]);
		BFS_SearchNode (); // 각 경로당 거리에대한 값 구함
		print ("" + startX + " " + startY + " " + minXX + " " + maxXX + " " + maxYY + " " + endX + " " + endY);
		Nearest_Road (startX, startY, minXX, maxXX, minYY, maxYY, endX, endY); // 최단경로 구함

	}
	void setEndPosition(int x, int y){
		Node [y,x].endPosition = true;
	}



}
