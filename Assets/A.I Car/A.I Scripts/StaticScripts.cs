﻿using UnityEngine;
using System.Collections;

public class StaticScripts{
	public static float MAX = -1;
	public static float MIN = 1000000;
	public static float MAXSPEED = 10;
	public static float MAXSTEER = 45;
	public static float MAXFORCE = 3f;
}
