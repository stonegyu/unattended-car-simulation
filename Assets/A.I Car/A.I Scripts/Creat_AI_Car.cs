﻿using UnityEngine;
using System.Collections;

public class Creat_AI_Car : MonoBehaviour {
	public GameObject AI_CAR;
	int idx = 0;
	AICar_DecideEndPoint DecideEndPointScript;

	public GameObject[] StartNode;
	public GameObject[] EndNode;
	public GameObject[] StartNode2;
	public GameObject[] EndNode2;
	public GameObject[] StartNode3;
	public GameObject[] EndNode3;
	public GameObject[] StartNode4;
	public GameObject[] EndNode4;
	void Start(){
		DecideEndPointScript = gameObject.GetComponent<AICar_DecideEndPoint> ();
	}
	float time = 0;
	void Update(){

		time += Time.deltaTime;
		if (time >= 10f) {
			time = 0;
			GameObject CopyCar = (GameObject)Instantiate(AI_CAR);
			AStar_NodeSearch thisAstarScript = CopyCar.GetComponentInChildren<AStar_NodeSearch>();
			DecideEndPointScript.Decide_StartEndNode();
			//thisAstarScript.CurrentNode = StartNode[idx%3];
			//int idxx = idx%3;
			//thisAstarScript.ArriveNode = EndNode[idxx];
			thisAstarScript.CurrentNode = DecideEndPointScript.getStartNode();
			thisAstarScript.ArriveNode = DecideEndPointScript.getEndNode();
			CopyCar.transform.position = new Vector3(thisAstarScript.CurrentNode.transform.position.x, 1f, thisAstarScript.CurrentNode.transform.position.z);
			thisAstarScript.FindFirstNode();
			thisAstarScript.GetShortestPathNode();
			CopyCar.name = "A.I_Car"+idx++;
		}


	}
	// Use this for initialization
	void OnGUI(){
		if(GUI.Button(new Rect(Screen.width-Screen.width/10, Screen.height-Screen.height/15, Screen.width/10, Screen.height/15),"Make Car")){
			GameObject CopyCar = (GameObject)Instantiate(AI_CAR);
			AStar_NodeSearch thisAstarScript = CopyCar.GetComponentInChildren<AStar_NodeSearch>();
			DecideEndPointScript.Decide_StartEndNode();
			//thisAstarScript.CurrentNode = StartNode[idx%3];
			//int idxx = idx%3;
			//thisAstarScript.ArriveNode = EndNode[idxx];
			thisAstarScript.CurrentNode = DecideEndPointScript.getStartNode();
			thisAstarScript.ArriveNode = DecideEndPointScript.getEndNode();
			CopyCar.transform.position = new Vector3(thisAstarScript.CurrentNode.transform.position.x, 1f, thisAstarScript.CurrentNode.transform.position.z);
			thisAstarScript.FindFirstNode();
			thisAstarScript.GetShortestPathNode();
			CopyCar.name = "A.I_Car"+idx++;
			/*
			//////////////////////////////////////////
			CopyCar = (GameObject)Instantiate(AI_CAR);
			thisAstarScript = CopyCar.GetComponentInChildren<AStar_NodeSearch>();
			//DecideEndPointScript.Decide_StartEndNode();
			thisAstarScript.CurrentNode = StartNode2[idx%3];

			thisAstarScript.ArriveNode = EndNode2[idx%3];
			CopyCar.transform.position = new Vector3(thisAstarScript.CurrentNode.transform.position.x, 1f, thisAstarScript.CurrentNode.transform.position.z);
			thisAstarScript.FindFirstNode();
			thisAstarScript.GetShortestPathNode();
			CopyCar.name = "A.I_Car";

			///////////////////////////////////////
			CopyCar = (GameObject)Instantiate(AI_CAR);
			thisAstarScript = CopyCar.GetComponentInChildren<AStar_NodeSearch>();
			DecideEndPointScript.Decide_StartEndNode();
			thisAstarScript.CurrentNode = StartNode3[idx%3];
			
			thisAstarScript.ArriveNode = EndNode3[idx%3];
			CopyCar.transform.position = new Vector3(thisAstarScript.CurrentNode.transform.position.x, 1f, thisAstarScript.CurrentNode.transform.position.z);
			thisAstarScript.FindFirstNode();
			thisAstarScript.GetShortestPathNode();
			CopyCar.name = "A.I_Car";

			////////////////////////////////////////// 
			CopyCar = (GameObject)Instantiate(AI_CAR);
			thisAstarScript = CopyCar.GetComponentInChildren<AStar_NodeSearch>();
			DecideEndPointScript.Decide_StartEndNode();
			thisAstarScript.CurrentNode = StartNode4[idx%3];
			
			thisAstarScript.ArriveNode = EndNode4[idx%3];
			CopyCar.transform.position = new Vector3(thisAstarScript.CurrentNode.transform.position.x, 1f, thisAstarScript.CurrentNode.transform.position.z);
			thisAstarScript.FindFirstNode();
			thisAstarScript.GetShortestPathNode();
			CopyCar.name = "A.I_Car"+idx++;
			*/
		}
	}
}
