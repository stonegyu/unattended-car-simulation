﻿using UnityEngine;
using System.Collections;

public class AICar_DecideEndPoint : MonoBehaviour {


	GameObject startNode = null;
	GameObject endNode = null;
	public GameObject[] startNodes;
	public GameObject[] EndNodes;
	public GameObject getStartNode(){
		return startNode;
	}
	public GameObject getEndNode(){
		return endNode;
	}

	public void Decide_StartEndNode(){
		startNode = startNodes[Random.Range (0, startNodes.Length - 1)].gameObject;
		float maxDistance = StaticScripts.MAX;
		for (int i = 0; i < EndNodes.Length; i++) {
			float distance = Vector3.Distance(startNode.transform.position, EndNodes[i].transform.position);
			if(maxDistance < distance){
				maxDistance = distance;
				endNode = EndNodes[i];
			}
		}
		print ("maxDistance : " + maxDistance);
	}
}