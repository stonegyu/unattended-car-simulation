﻿using UnityEngine;
using System.Collections;

public class AI_MotionControl : MonoBehaviour {
	//GameObject[] Node;

	public GameObject SearchNode = null;
	public GameObject BeforeNode = null;
	public GameObject Center = null;
	GameObject LocalObj = null;
	GameObject thisLocObj = null;
	string BeforeNodeName = "";
	Vector3 AIPosition, NodePosition;
	public GameObject Direction;
	public AStar_NodeSearch MyAstar_NodeSearch;
	bool isArrive = false;

	bool ArriveOn = false;
	bool SeekOn = false;

	public bool LaneChangeLeft = false;
	public bool LaneChangeRight = false;
	//Vector3 CollisionDetectionPos = Vector3.zero;

	Quaternion BasicQuaternion = new Quaternion(0,0,0,0);


	public bool getLaneChangeLeft(){
		return LaneChangeLeft;
	}
	public bool getLaneChangeRight(){
		return LaneChangeRight;
	}
	void SetgameObjectForward(){
		BeforeNode = MyAstar_NodeSearch.getCurrentNode ();
		SearchNode = MyAstar_NodeSearch.getCurrentNode ();
		if (BeforeNode.name.Equals (SearchNode.name)) {
			gameObject.transform.forward = (SearchNode.transform.position - BeforeNode.transform.position);
		} else {
			Transform parent = SearchNode.transform.parent;
			NodeInformation[] childs = parent.GetComponentsInChildren<NodeInformation>();
			for(int i = 0; i < childs.Length; i++){
				if(childs[i].name.Equals(BeforeNode.name)){
					gameObject.transform.forward = (childs[i].transform.position - BeforeNode.transform.position);
					break;
				}
			}
		}
		Center.transform.rotation = gameObject.transform.rotation;
		BasicQuaternion = Center.transform.rotation;
	}
	void Start () {
		MyAstar_NodeSearch = gameObject.GetComponent<AStar_NodeSearch> ();

		AICar_DecideEndPoint DecideEndPointScript = GameObject.Find("MainCamera").GetComponent<AICar_DecideEndPoint> ();
		AStar_NodeSearch thisAstarScript = gameObject.GetComponentInChildren<AStar_NodeSearch>();

		thisLocObj = (GameObject)Instantiate (Direction);
		thisLocObj.transform.parent = gameObject.transform;
		LocalObj = (GameObject)Instantiate (Direction);
		LocalObj.transform.parent = Center.transform;
		//테스트 모드일때만 쓰임 (버튼 클릭은 Create_AI_Car 에서 설정 )
		//thisAstarScript.CurrentNode = DecideEndPointScript.getStartNode();
		//thisAstarScript.FindFirstNode();
		//thisAstarScript.GetShortestPathNode();
	}

	void FixedUpdate () {

		Center.transform.position = gameObject.transform.position;
		Center.transform.rotation = gameObject.transform.rotation;
		if(ExceptionFuction () == true) return;
		                                                
		if (NextNodeAllocation() == false)return;

		ActionSelect ();
		UpdateVelocity ();
		Debug.DrawLine (transform.position, Direction.transform.position);
	}

	float fabs(float n){
		return (n < 0) ? n * -1 : n;
	}

	bool settings = false;
	bool ExceptionFuction(){
		if (MyAstar_NodeSearch.getIsInitProcessDone () == false) {
			// 경로탐색이 끝난뒤 움직여야 함
			return true;
		} else {
			if(settings == false){
				settings=true;
				SetgameObjectForward (); // Search, Before 할당 / 자신이 갈경로의 정면을 바라보도록함 Center로테이션도 맞춰줌
			}
		}
		return false;
	}
	bool NextNodeAllocation(){
		Vector3 LocSearchNodePos = GetLocPosition (SearchNode.transform.position);
		if (LocSearchNodePos.z > 2f || !SearchNode.GetComponent<NodeInformation>().IsGo) {
			UpdateVelocity();
			return true;
		}
			

		if(isArrive == true){
			// 도착경로는 다음노드를 판단할 시점에서 경로탐색 큐가 비었는지를 확인함
			if(LocSearchNodePos.z > 0.1f) {
				Destroy(gameObject);
			}
			UpdateVelocity();
			return false;
		}
		BeforeNode = SearchNode;
		SearchNode = gameObject.GetComponent<AStar_NodeSearch>().getCurrentNode();
		isArrive = gameObject.GetComponent<AStar_NodeSearch>().isLastNode();
		return true;
	}

	Vector3 GetLocPosition(Vector3 pos){
		LocalObj.transform.position = pos;
		return LocalObj.transform.localPosition;
	}
	Vector3 GetThisPosition(Vector3 pos){
		thisLocObj.transform.position = pos;
		return LocalObj.transform.localPosition;
	}
	void ActionChange(string action){
		if (action.Equals ("Seek")) {
			SeekOn = true;
			ArriveOn = false;
			return;
		}
		if (action.Equals ("Arrive")) {
			SeekOn = false;
			ArriveOn = true;
			return;
		}
	}

	void LaneChangeCalculate(){


		Vector3 SearchLocPos = GetLocPosition (SearchNode.transform.position);

		int BeforeNodeNum = int.Parse(BeforeNode.name.Split ('_') [1]);
		int SearchNodeNum = int.Parse(SearchNode.name.Split ('_') [1]);
		if (fabs(SearchLocPos.x) < 0.5f || BeforeNodeNum == SearchNodeNum) {
			LaneChangeLeft = false;
			LaneChangeRight = false;
			return;
		}
		if (BeforeNode.name.Equals (SearchNode.name)) {
			Center.transform.forward = (SearchNode.transform.position - BeforeNode.transform.position);
		} else {
			Transform parent = SearchNode.transform.parent;
			NodeInformation[] childs = parent.GetComponentsInChildren<NodeInformation>();
			for(int i = 0; i < childs.Length; i++){
				if(childs[i].name.Equals(BeforeNode.name)){
					Center.transform.forward = (childs[i].transform.position - BeforeNode.transform.position);
					break;
				}
			}
		}
		//Center.transform.rotation = BasicQuaternion;
		//왼쪽에 서치 포지션이 있을때
		if (SearchLocPos.x < 0) {
			LaneChangeLeft = true;
			LaneChangeRight = false;
			return;
		}

		//오른쪽에 서치 포지션이 있을때
		if (SearchLocPos.x > 0) {
			LaneChangeLeft = false;
			LaneChangeRight = true;
		}
	}
	void ActionSelect(){
		GameObject[] Agents = GameObject.FindGameObjectsWithTag ("A.I_Car");
		GameObject ForwardNearestAgent = null;
		GameObject LeftNearestAgent = null;
		GameObject RightNearestAgent = null;
		float ForwardNearestDist = StaticScripts.MIN;
		float LeftNearestDist = StaticScripts.MIN;
		float RightNearestDist = StaticScripts.MIN;

		RaycastHit hit;
		for (int i = 0; i < 360; i++) {
			float x = Mathf.Cos (i*Mathf.Deg2Rad);
			float z = Mathf.Sin (i*Mathf.Deg2Rad);
			if(Physics.Raycast(transform.position, new Vector3(x,0,z), out hit, rigidbody.velocity.magnitude+5f)){
				if(hit.collider.gameObject.GetComponent<AI_MotionControl>()){
					GameObject Agent = hit.collider.gameObject;
					Vector3 AgentLocPos = GetLocPosition(Agent.transform.position);
					if(AgentLocPos.z >= 0){
						//정면 우면 좌면 각각 나눠서 해야함
						float dist = Vector3.Distance(transform.position, Agent.transform.position);

						if (fabs (AgentLocPos.x) <= transform.localScale.x*2) {
							if(ForwardNearestDist > dist){
								ForwardNearestDist  = dist;
								ForwardNearestAgent = Agent;
							}
						}

						if(transform.localScale.x*2 < AgentLocPos.x){
							if(RightNearestDist > dist){
								RightNearestDist  = dist;
								RightNearestAgent = Agent;
							}
						}

						if(transform.localScale.x*-2 > AgentLocPos.x){
							if(LeftNearestDist > dist){
								LeftNearestDist  = dist;
								LeftNearestAgent = Agent;
							}
						}
						//Debug.DrawLine(transform.position, hit.point, Color.yellow);
					}
				}
			}
		}


		//CollisionDetectionPos = Vector3.zero;
		//라인 체인지가 일어나는지 확인해줘야함
		LaneChangeCalculate ();

		//가까이있는 애들이 없기떄문에 목표지점을 보고 가야함
		Direction.transform.position = SearchNode.transform.position;
		Vector3 SearchLocPos = GetLocPosition (SearchNode.transform.position);
		float limit_z = (rigidbody.velocity.magnitude+5f<SearchLocPos.z-1.5f)?rigidbody.velocity.magnitude+5f : SearchLocPos.z-1.5f; 
		Direction.transform.localPosition = new Vector3(Direction.transform.localPosition.x,
		                                                0,
		                                                limit_z);
		ActionChange("Seek");


		if (ForwardNearestAgent == null && LeftNearestAgent == null && RightNearestAgent == null) {

			if(isArrive){
				ActionChange("Arrive");
			}else{
				if(SearchNode.GetComponent<NodeInformation>().IsGo){
					ActionChange("Seek");
				}else{
					ActionChange("Arrive");
				}

			}
			return;
		}
		if(ForwardNearestAgent != null){
			Vector3 NearestAgentLocPos = GetLocPosition (ForwardNearestAgent.transform.position);
			Vector3 NearestAgentForwardPos = GetThisPosition (ForwardNearestAgent.transform.position);


			if (fabs (NearestAgentForwardPos.x) <= transform.localScale.x*2) {
				//정면에 있음 안전거리만큼 SearchNode를 ArrivePoint에 넣음
				//CollisionDetectionPos = ForwardNearestAgent.transform.position;
				Direction.transform.position = SearchNode.transform.position;

				Direction.transform.localPosition = new Vector3(Direction.transform.localPosition.x,
				                                                0,
				                                                (NearestAgentLocPos.z-7f <= 0)?0 : NearestAgentLocPos.z-7f);
				//(NearestAgentLocPos.z-5f <= 0)?0 : NearestAgentLocPos.z-5f)
				Debug.DrawLine(transform.position, Direction.transform.position);
				ActionChange("Arrive");
				return;
			}

		}

		if(LeftNearestAgent != null){
			Vector3 NearestAgentLocPos = GetLocPosition (LeftNearestAgent.transform.position);
			Vector3 NearestAgentForwardPos = GetThisPosition (LeftNearestAgent.transform.position);


			if ( NearestAgentLocPos.x < 0) {
				// 정면이 아니고 오른쪽에 있다고 판단이됨 
				// 그럼 내가 오른쪽으로 가는지 아니면 상대방이 왼쪽으로 오는지를 확인해야함
				if(LaneChangeRight || LeftNearestAgent.GetComponent<AI_MotionControl>().getLaneChangeLeft()){
					//CollisionDetectionPos = LeftNearestAgent.transform.position;
					Direction.transform.position = SearchNode.transform.position;
					Direction.transform.localPosition = new Vector3(Direction.transform.localPosition.x,
					                                                0,
					                                                (NearestAgentLocPos.z-7f <= 0)?0 : NearestAgentLocPos.z-7f);
					Debug.DrawLine(transform.position, Direction.transform.position, Color.yellow);
					ActionChange("Arrive");
				}
				return;
			}

		}
		
		if(RightNearestAgent != null){
			Vector3 NearestAgentLocPos = GetLocPosition (RightNearestAgent.transform.position);
			Vector3 NearestAgentForwardPos = GetThisPosition (RightNearestAgent.transform.position);


			if (NearestAgentLocPos.x > 0) {
				// 정면이 아니고 오른쪽에 있다고 판단이됨 
				// 그럼 내가 오른쪽으로 가는지 아니면 상대방이 왼쪽으로 오는지를 확인해야함
				if(LaneChangeLeft || RightNearestAgent.GetComponent<AI_MotionControl>().getLaneChangeRight()){
					//CollisionDetectionPos = RightNearestAgent.transform.position;
					Direction.transform.position = SearchNode.transform.position;
					Direction.transform.localPosition = new Vector3(Direction.transform.localPosition.x,
					                                                0,
					                                                (NearestAgentLocPos.z-7f <= 0)?0 : NearestAgentLocPos.z-7f);
					
					ActionChange("Arrive");
					Debug.DrawLine(transform.position, Direction.transform.position, Color.yellow);
				}
				return;
			}

		}




	}



	void UpdateVelocity(){
		Vector3 SteeringForce = Calculate ();
		Vector3 acceleration = SteeringForce / rigidbody.mass;
		Vector3 Velocity = rigidbody.velocity;
		Velocity += acceleration * Time.deltaTime;
		Velocity = Truncate (Velocity, StaticScripts.MAXSPEED);
		Velocity.y = 0;
		rigidbody.velocity = Velocity;

		Vector3 locSearch = GetLocPosition (SearchNode.transform.position);

		if (locSearch.z > 0) {
			Vector3 relative = transform.InverseTransformPoint (Direction.transform.position);
			gameObject.GetComponent<AI_Car> ().setHandle (Mathf.Atan2 (relative.x, relative.z));
		} else {
			Vector3 relative = transform.InverseTransformPoint (SearchNode.transform.position);
			gameObject.GetComponent<AI_Car> ().setHandle (Mathf.Atan2 (relative.x, relative.z));
		}
	}
	Vector3 Truncate(Vector3 limitVector, float max){
		float length = Mathf.Sqrt (limitVector.x * limitVector.x + limitVector.z * limitVector.z);
		if (length > max) {
			limitVector = limitVector.normalized;
			limitVector *= max;
		}
		return limitVector;
	}
	Vector3 Calculate(){
		Vector3 SteeringForce = Vector3.zero;
		if (SeekOn) {
			Vector3 force = Seek(Direction.transform.position);
			SteeringForce = AccumulateForce(SteeringForce, force);
		}
		if (ArriveOn) {
			Vector3 force = Arrive (Direction.transform.position,Deceleration.slow);
			SteeringForce = AccumulateForce(SteeringForce, force);
		}
		Vector3 loc = GetLocPosition (Direction.transform.position);
		if (loc.z < 0) {
			return Vector3.zero;
		}
		//SteeringForce += CollisionAvoidance (CollisionDetectionPos);
		return Truncate (SteeringForce, StaticScripts.MAXFORCE);
	}
	Vector3 AccumulateForce(Vector3 RunningTot, Vector3 ForceToAdd){
		float MagnitudeSoFar = Mathf.Sqrt(RunningTot.x*RunningTot.x + RunningTot.z*RunningTot.z);
		float MagnitudeRemaining = StaticScripts.MAXFORCE - MagnitudeSoFar;
		if (MagnitudeRemaining <= 0.0)
			return RunningTot;
		float MagnitudeToAdd = Mathf.Sqrt (ForceToAdd.x * ForceToAdd.x + ForceToAdd.z * ForceToAdd.z);

		if (MagnitudeToAdd < MagnitudeRemaining) {
			RunningTot += ForceToAdd;
		} else {
			RunningTot += (ForceToAdd.normalized * MagnitudeRemaining);
		}
		return RunningTot;
	}

	Vector3 Seek(Vector3 TargetPos){
		Vector3 DesireVelocity = (TargetPos - transform.position).normalized * StaticScripts.MAXSPEED;
		return (DesireVelocity - rigidbody.velocity);
	}

	enum Deceleration{slow = 3, normal =2, fast = 1};
	Vector3 Arrive(Vector3 TargetPos, Deceleration deceleration){
		Vector3 ToTarget = TargetPos - transform.position;
		float dist = Mathf.Sqrt (ToTarget.x * ToTarget.x + ToTarget.z * ToTarget.z);
		if (dist > 0) {
			const float DecelerationTweaker = 0.3f;
			float speed = dist / ((float)deceleration * DecelerationTweaker);
			speed = (speed > StaticScripts.MAXSPEED)?StaticScripts.MAXSPEED:speed;
			Vector3 DesiredVelocity = ToTarget * speed / dist;
			return (DesiredVelocity - rigidbody.velocity);
		}
		return Vector3.zero;
	}
	/*
	Vector3 CollisionAvoidance(Vector3 TargetPos)
	{
		Vector3 SteeringForce = (rigidbody.mass * rigidbody.mass)/
			((transform.position - TargetPos)*(transform.position - TargetPos));
		return SteeringForce;
	}
	*/
}
