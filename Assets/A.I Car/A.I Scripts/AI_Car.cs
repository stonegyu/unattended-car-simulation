﻿using UnityEngine;
using System.Collections;

public class AI_Car : MonoBehaviour {
	WheelCollider FL_Wheel, FR_Wheel, BL_Wheel, BR_Wheel;
	
	Transform FL_Pos, FR_Pos;
	
	Transform FL_Mesh, FR_Mesh, BL_Mesh, BR_Mesh;

	public Vector3 center_of_mesh;
	float steer = 0;
	// Use this for initialization
	void Start () {
		FL_Wheel = transform.FindChild ("Wheel/Wheel_FL/Wheel_FL_Collider").transform.collider as WheelCollider;
		FR_Wheel = transform.FindChild ("Wheel/Wheel_FR/Wheel_FR_Collider").transform.collider as WheelCollider;
		BL_Wheel = transform.FindChild ("Wheel/Wheel_BL/Wheel_BL_Collider").transform.collider as WheelCollider;
		BR_Wheel = transform.FindChild ("Wheel/Wheel_BR/Wheel_BR_Collider").transform.collider as WheelCollider;
		
		FL_Pos = transform.FindChild("Wheel/Wheel_FL/Wheel_FL_Collider").transform;
		FR_Pos = transform.FindChild("Wheel/Wheel_FR/Wheel_FR_Collider").transform;
		
		FL_Mesh = transform.FindChild("Wheel/Wheel_FL/Wheel_FL_Collider/Wheel_FL_Mesh").transform;
		FR_Mesh = transform.FindChild("Wheel/Wheel_FR/Wheel_FR_Collider/Wheel_FR_Mesh").transform;
		BL_Mesh = transform.FindChild("Wheel/Wheel_BL/Wheel_BL_Collider/Wheel_BL_Mesh").transform;
		BR_Mesh = transform.FindChild("Wheel/Wheel_BR/Wheel_BR_Collider/Wheel_BR_Mesh").transform;
		
		rigidbody.centerOfMass = center_of_mesh; // 차량의 무게중심이 Ground에 가깝지 않으면 주행 중 뒤집어 지거나 정상적인 주행이 불가함
	}
	float fabs(float n){
		return (n < 0) ? n * -1 : n;
	}
	public void setHandle(float steer){
		float buho = 1;
		if(steer<0) buho = -1;
		if (fabs(steer) > 1) steer = 1*buho;
		this.steer = steer;

	}
	void FixedUpdate(){
		float rpm = fabs(rigidbody.velocity.magnitude * 60f / 2f / 3.141582f); //rpm 유니티 내장함수

		FL_Pos.localEulerAngles = new Vector3 (0, steer * StaticScripts.MAXSTEER, 0);
		FR_Pos.localEulerAngles = new Vector3 (0, steer * StaticScripts.MAXSTEER, 0);

		FL_Mesh.Rotate (rpm * Time.deltaTime, 0, 0);
		FR_Mesh.Rotate (rpm * Time.deltaTime, 0, 0);
		BL_Mesh.Rotate (rpm * Time.deltaTime, 0, 0);
		BR_Mesh.Rotate (rpm * Time.deltaTime, 0, 0);

		//speed = Mathf.Abs( 1f * 2 * 3.14159f * FL_Wheel.rpm / 60 ); //타이어 지름 * 2 (바퀴 2개) * 원주율 * rpm/60

	}
}
