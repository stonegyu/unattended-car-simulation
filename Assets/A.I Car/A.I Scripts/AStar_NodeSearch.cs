﻿using UnityEngine;
using System.Collections;

public class AStar_NodeSearch : MonoBehaviour {
	public GameObject CurrentNode = null;
	public GameObject ArriveNode;
	//public int CurrentNodeNumber = -1;
	public Queue ShortestPathNodes = new Queue();
	bool IsinitProcessDone = false;

	public bool getIsInitProcessDone(){
		return IsinitProcessDone;
	}
	public GameObject getCurrentNode(){
		if(ShortestPathNodes.Count ==0) return null;
		return (GameObject)ShortestPathNodes.Dequeue();
	}
	public bool isLastNode(){
		if(ShortestPathNodes.Count == 0) return true;
		return false;
	}
	public void FindFirstNode(){
		IsinitProcessDone = false;

		GameObject[] Nodes_0 = GameObject.FindGameObjectsWithTag ("0");
		float NearestNodeDistance = StaticScripts.MIN;

		for(int i = 0; i < Nodes_0.Length; i++){
			NodeInformation[] Nodes_0_NodeInformations = Nodes_0[i].transform.GetComponentsInChildren<NodeInformation>();
			// 연결된 자식노드를 탐색 
			for(int j = 0; j < Nodes_0_NodeInformations.Length; j++){
				GameObject curNode = Nodes_0_NodeInformations[j].gameObject;
				float nodeDistance = Vector3.Distance(curNode.transform.position, transform.position);
				if(NearestNodeDistance > nodeDistance){
					NearestNodeDistance = nodeDistance;
					CurrentNode = curNode;
				}
			}
		}
	}

	public GameObject[] DebugArray = new GameObject[50];
	public void GetShortestPathNode(){

		GameObject SearchCurrentNode = CurrentNode.gameObject;
		ShortestPathNodes.Enqueue (CurrentNode);
		int debugi = 0;
		while(SearchCurrentNode.Equals(ArriveNode) == false){

			float NearestNodeDistance = StaticScripts.MIN;
			NodeInformation CurrentNodeInfo = SearchCurrentNode.GetComponent<NodeInformation> ();
			GameObject[] NextNodeArray = CurrentNodeInfo.NextNode;
			GameObject NextNode = CurrentNode;

			for(int i = 0; i < NextNodeArray.Length; i++){
				Vector2 from = new Vector2(NextNodeArray[i].transform.position.x, NextNodeArray[i].transform.position.z);
				Vector2 to = new Vector2(ArriveNode.transform.position.x, ArriveNode.transform.position.z);
				float curdistance = Vector3.Distance(NextNodeArray[i].transform.position, SearchCurrentNode.transform.position);

				float distance = Vector2.Distance(from, to)+curdistance;
				if(distance < NearestNodeDistance){
					NextNode = NextNodeArray[i];
					NearestNodeDistance = distance;
				}
			}
			SearchCurrentNode = NextNode;
			DebugArray[debugi++] = SearchCurrentNode;

			ShortestPathNodes.Enqueue(SearchCurrentNode);
		}

		IsinitProcessDone = true;
	}
	// Use this for initialization
	void Start () {
		//FindFirstNode (); // 최근접 노드를 탐색
		//GetShortestPathNode ();
	}
}
