﻿using UnityEngine;
using System.Collections;

public class csCarMove : MonoBehaviour {
	WheelCollider FL_Wheel, FR_Wheel, BL_Wheel, BR_Wheel;

	Transform FL_Pos, FR_Pos;

	Transform FL_Mesh, FR_Mesh, BL_Mesh, BR_Mesh;

	public Vector3 center_of_mesh;

	public float max_steer = 30; //최대 회전(커브)
	public float max_torque = 20; // 엔진의 파워
	public float max_break = 1; // 브레이크 파워

	float car_moter = 0;
	float car_break = 0;

	float forward = 0;
	float back = 0;

	float current_speed = 0; 
	public float steer = 0;

	public static float speed = 0; //실제 속도

	float go_max_speed = 120;
	float back_max_speed = 25;
	float nomal_steer = 0.8f;
	// Use this for initialization
	void Start () {
		FL_Wheel = transform.FindChild ("Wheel/Wheel_FL/Wheel_FL_Collider").transform.collider as WheelCollider;
		FR_Wheel = transform.FindChild ("Wheel/Wheel_FR/Wheel_FR_Collider").transform.collider as WheelCollider;
		BL_Wheel = transform.FindChild ("Wheel/Wheel_BL/Wheel_BL_Collider").transform.collider as WheelCollider;
		BR_Wheel = transform.FindChild ("Wheel/Wheel_BR/Wheel_BR_Collider").transform.collider as WheelCollider;

		FL_Pos = transform.FindChild("Wheel/Wheel_FL/Wheel_FL_Collider").transform;
		FR_Pos = transform.FindChild("Wheel/Wheel_FR/Wheel_FR_Collider").transform;

		FL_Mesh = transform.FindChild("Wheel/Wheel_FL/Wheel_FL_Collider/Wheel_FL_Mesh").transform;
		FR_Mesh = transform.FindChild("Wheel/Wheel_FR/Wheel_FR_Collider/Wheel_FR_Mesh").transform;
		BL_Mesh = transform.FindChild("Wheel/Wheel_BL/Wheel_BL_Collider/Wheel_BL_Mesh").transform;
		BR_Mesh = transform.FindChild("Wheel/Wheel_BR/Wheel_BR_Collider/Wheel_BR_Mesh").transform;

		rigidbody.centerOfMass = center_of_mesh; // 차량의 무게중심이 Ground에 가깝지 않으면 주행 중 뒤집어 지거나 정상적인 주행이 불가함
	}

	void FixedUpdate(){
		current_speed = rigidbody.velocity.sqrMagnitude;
		
		steer = Input.GetAxis ("Horizontal") * ( 1 - speed / (go_max_speed * 1.3f) ) * (nomal_steer); // Input.GetAxis 범위 -1 ~ 1
		//
		forward = Mathf.Clamp (Input.GetAxis ("Vertical"), 0, 1); // Mathf.clamp("현재값","최대값","최소값") 
																  // 현재값이 최대값보다 크거나 그 최대값까지만 반환 
		back = Mathf.Clamp (Input.GetAxis ("Break"), 0, 1)*-1;   // 최소값보다 작으면 그 최소값으로만 반환


		float rpm = FL_Wheel.rpm; //rpm 유니티 내장함수

		FL_Wheel.motorTorque = max_torque * car_moter; //motorToqur, brakeToqur는 유니티 내장함수
		FR_Wheel.motorTorque = max_torque * car_moter;

		BL_Wheel.brakeTorque = max_break * car_break;
		BR_Wheel.brakeTorque = max_break * car_break;

		FL_Pos.localEulerAngles = new Vector3 (0, steer * max_steer, 0);
		FR_Pos.localEulerAngles = new Vector3 (0, steer * max_steer, 0);
		print ("back : "+back+" forward : "+ Input.GetAxis ("Vertical") +" steer = " + steer +"max_steer "+max_steer);

		FL_Mesh.Rotate (rpm * Time.deltaTime, 0, 0);
		FR_Mesh.Rotate (rpm * Time.deltaTime, 0, 0);
		BL_Mesh.Rotate (rpm * Time.deltaTime, 0, 0);
		BR_Mesh.Rotate (rpm * Time.deltaTime, 0, 0);

		bool go_forward = true;

		if(current_speed <= 0.1f)
		{
			if(back < 0)
				go_forward = false; //현재 뒤로가고 있는 상황
			if(forward > 0)
				go_forward = true; //현재 앞으로 가고 있는 상황
		}
		if(Input.GetAxis("Horizontal") < 0 || Input.GetAxis("Horizontal") >0){ //커브를 할 때 속도가 줄어들기 위함
			if(go_forward == true){
				car_break = 0.4f * (0.5f + speed / go_max_speed);
			}else if(go_forward == false){
				car_break = 0.2f * (0.5f + speed / go_max_speed);
			}
		}
		if(go_forward == true){ // 가속=가속, 후진=브레이크
			car_moter = forward;
			car_break = -back;

			if(speed >=120){ //전진 최대속도 제한
				current_speed = 120;
				car_moter=0;
			}
			if(current_speed >= 0 && forward == 0 && back == 0){ //아무런 이벤트를 받지 않으면 정지
				car_break=2;
			}
		}
		else if(go_forward == false){ //가속 = 감속 후진 = 후진으로 가속
			car_moter = back;
			car_break = forward;
			if(speed >=20){ //후진 최대속도 제한
				current_speed = 20;
				car_moter=0;
			}
			if(current_speed >= 0 && forward == 0 && back == 0){ //아무런 이벤트를 받지 않으면 정지
				car_break=1;
			}
		}
		speed = Mathf.Abs( 1f * 2 * 3.14159f * FL_Wheel.rpm / 60 ); //타이어 지름 * 2 (바퀴 2개) * 원주율 * rpm/60
		//speed = current_speed;
		//print ("speed : "+speed);
	}
	// Update is called once per frame
	void Update () {


	}
}
