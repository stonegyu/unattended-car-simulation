﻿using UnityEngine;
using System.Collections;

public class MeterContoroller : MonoBehaviour {

	Transform arrow;
	
	void Start()
	{
		arrow = transform.FindChild("Speed_Arrow").transform;
	}
	
	void Update()
	{
		float angle_x = csCarMove.speed * 45 / 25 - 135;		
		arrow.localEulerAngles = new Vector3(0, angle_x, 0);
	}

}
