﻿using UnityEngine;
using System.Collections;

public class Moving_Car : MonoBehaviour {

	Vector3 Heading;

	float Mass;
	float MaxSpeed, Current_Speed;
	float MaxForce;
	float CurrentRotate_Degree;
	bool IsLeft, IsRight, IsForword, IsBackword;
	float before_rad=0;
	public GameObject Front_Left_Wheel, Front_Right_Wheel, Back_Left_Wheel, Back_Right_Wheel; 
	// Use this for initialization
	private void Car_Init_Setting(){
		Mass = 1290.0f;
		gameObject.rigidbody.mass = Mass;
		MaxForce = Mass*2f;
		MaxSpeed = 3f;
		Heading = gameObject.transform.forward;
		CurrentRotate_Degree = 0;
		Current_Speed = 0;
		IsLeft = false; IsRight = false; IsForword = false; IsBackword = false;
	}
	void Start () {
		Car_Init_Setting ();
	}

	private float Ristrict_Degree(float Updated_Degree){
		if(IsLeft == true && Updated_Degree <-45){
			Updated_Degree = -45;
		}else if(IsRight == true && Updated_Degree > 45){
			Updated_Degree = 45;
		}
		return Updated_Degree;
	}
	private float UpdateDegree(){
		if(!IsLeft == true && !IsRight == true){
			return 0;
		}

		if(IsLeft == true){
			CurrentRotate_Degree=-20f;
		}else if(IsRight == true){
			CurrentRotate_Degree=20f;
		}
		CurrentRotate_Degree = Ristrict_Degree (CurrentRotate_Degree);

		return CurrentRotate_Degree;
	}

	private bool isSame_Cur_Bef_WheelDirection(float rad){
		if (before_rad > 0 && rad > 0)
			return true;
		else if(before_rad<0 && rad <0){
			return true;
		}
		return false;
	}
	private void ChangeWheelDirection(float rad){
		if(!IsLeft == true && !IsRight == true){
			Front_Left_Wheel.transform.rotation = Quaternion.Slerp(Front_Left_Wheel.transform.rotation,
			                                                       gameObject.transform.rotation,
			                                                       Time.deltaTime * 2.0f);
			Front_Right_Wheel.transform.rotation = Quaternion.Slerp(Front_Left_Wheel.transform.rotation,
			                                                       gameObject.transform.rotation,
			                                                       Time.deltaTime * 2.0f);
		}

		float dot = Vector3.Dot (Heading.normalized, Front_Left_Wheel.transform.forward.normalized);
		float Wheel_Degree = Mathf.Acos (dot) * Mathf.Rad2Deg;
		bool isSame = isSame_Cur_Bef_WheelDirection(rad);

		if(Wheel_Degree>45 && isSame){
			Front_Left_Wheel.transform.Rotate(0f, 0f, 0f);
			Front_Right_Wheel.transform.Rotate(0f, 0F, 0f);
			return;
		}

		Front_Left_Wheel.transform.Rotate(0f, Mathf.Sin (rad), 0f);
		Front_Right_Wheel.transform.Rotate(0f, Mathf.Sin (rad), 0f);
		before_rad = rad;
	}
	private void Update_Car_Rotate(){
		CurrentRotate_Degree = UpdateDegree ();
		float rad = CurrentRotate_Degree * Mathf.Deg2Rad;
		ChangeWheelDirection (rad);
	}
	private void Update_Car_Heading(){
		Heading = gameObject.transform.forward;
	}

	private float Ristrict_MaxVelocity(Vector3 BeforeForce){

		if((gameObject.rigidbody.velocity.magnitude > MaxSpeed) && (Heading.normalized==BeforeForce)){
			Vector3 Nomalized = gameObject.rigidbody.velocity.normalized;
			float DirectionSpeed = MaxSpeed*DecideDirection ();
			gameObject.rigidbody.velocity = new Vector3(Nomalized.x*DirectionSpeed,
			                                            Nomalized.y*DirectionSpeed,
			                                            Nomalized.z*DirectionSpeed);
		}
		return gameObject.rigidbody.velocity.magnitude;
	}

	private bool Check_Push_Accelation (){
		if (!IsForword == true && !IsBackword == true){
			Current_Speed = gameObject.rigidbody.velocity.magnitude;
			gameObject.rigidbody.AddForce (0, 0, 0);
			return true;
		}
		return false;
	}
	private int DecideDirection(){
		int Direction = 1;
		if (IsBackword == true)	Direction = -1; 
		return Direction;
	}
	private Vector3 UpdateVelocity(){

		float DirectionForce = MaxForce*DecideDirection ();
		float BeforeVelocity = gameObject.rigidbody.velocity.magnitude;
		Vector3 Force = new Vector3(Heading.x * DirectionForce,
		                            Heading.y * DirectionForce, 
		                            Heading.z * DirectionForce);
		gameObject.rigidbody.AddForce (Force);
		return Force.normalized;
	}
	private void Update_Car_Forword(){
		if(Check_Push_Accelation ()) return;
		
		Current_Speed = Ristrict_MaxVelocity (UpdateVelocity ());
	}
	void Input_Key_Code(){
		if (Input.GetKeyDown (KeyCode.LeftArrow))IsLeft = true;
		else if(Input.GetKeyUp (KeyCode.LeftArrow)) IsLeft = false;

		if (Input.GetKeyDown (KeyCode.RightArrow))IsRight = true;
		else if(Input.GetKeyUp (KeyCode.RightArrow)) IsRight = false;

		if (Input.GetKeyDown (KeyCode.UpArrow))IsForword = true;
		else if(Input.GetKeyUp (KeyCode.UpArrow))IsForword = false;

		if (Input.GetKeyDown (KeyCode.DownArrow))IsBackword = true;
		else if (Input.GetKeyUp (KeyCode.DownArrow)) IsBackword = false;
	}


	// Update is called once per frame
	void Update () {
		Input_Key_Code (); // 유저 Input key
		Update_Car_Rotate (); // Input key에 따른 자동차 회전 로직
		Update_Car_Heading (); // Rotate에 따른 자동차 Heading 설정
		Update_Car_Forword (); // Input key에 따른 자동차 속도 로직
	}

}
