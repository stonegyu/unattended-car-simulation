﻿using UnityEngine;
using System.Collections;

public class LightManager : MonoBehaviour {
	Light Front_0, Front_1;
	Light Back_0, Back_1;
	bool FrontLight_OnOff = false;
	csCarMove MyCar;
	// Use this for initialization
	void Start () {
		Front_0 = transform.FindChild ("Front_Light_0").light;
		Front_1 = transform.FindChild ("Front_Light_1").light;
		Front_0.enabled = FrontLight_OnOff;
		Front_1.enabled = FrontLight_OnOff;
		Back_0 = transform.FindChild ("Back_Light_0").light;
		Back_1 = transform.FindChild ("Back_Light_1").light;
		Back_0.enabled = false;
		Back_1.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.F)){
			FrontLight_OnOff = !FrontLight_OnOff;
			Front_0.enabled = FrontLight_OnOff;
			Front_1.enabled = FrontLight_OnOff;
				
		}

		if(Input.GetAxis("Vertical") > 0){
			Back_0.enabled = false;
			Back_1.enabled = false;
		}else if(Input.GetAxis("Vertical") < 0){
			Back_0.enabled = true;
			Back_1.enabled = true;
		}else if(Input.GetAxis("Vertical") == 0){
			Back_0.enabled = true;
			Back_1.enabled = true;
		}
	}
}
