﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CameraManager : MonoBehaviour {
	GameObject Agent = null;
	GameObject[] Agents;
	int CameraIdx = 0;
	public GameObject CameraTotalViewPos, CameraTotalViewLookAtPos;
	int CameraMode = 0;
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		SetAutoCameraPos ();
		if (Input.GetMouseButtonUp (0)) {
			GetAgentsList();

		}
		if(Input.GetKeyDown(KeyCode.C)){
			CameraMode = (CameraMode + 1) % 3;
		}
	}
	void GetAgentsList()
	{
		Agents = GameObject.FindGameObjectsWithTag ("A.I_Car");
		if (Agents.Length == 0)
			return;
		ReAllocAgent();
	}
	void ReAllocAgent()
	{
		CameraIdx = (CameraIdx + 1) % Agents.Length;
		Agent = Agents [CameraIdx];
	}
	void SetAutoCameraPos()
	{
		if (Agent == null) {
			GetAgentsList();
			return;
		}
		if (CameraMode == 0) {
			transform.position = new Vector3 (Agent.transform.position.x + 20, Agent.transform.position.y + 20, Agent.transform.position.z +10);
			transform.LookAt (Agent.transform.position);
			transform.RotateAround (Agent.transform.position, Vector3.up, Agent.transform.rotation.x);
		} else if (CameraMode == 1) {
			transform.position = CameraTotalViewPos.transform.position;
			transform.LookAt (CameraTotalViewLookAtPos.transform.position);
			transform.RotateAround (CameraTotalViewLookAtPos.transform.position, Vector3.up, CameraTotalViewLookAtPos.transform.rotation.z);
		} else if (CameraMode == 2) {
			transform.position = new Vector3 (Agent.transform.position.x, Agent.transform.position.y+1.1f, Agent.transform.position.z);
			transform.rotation = Agent.transform.rotation;
		}

	}
}
//-858  263.2  -1121.1
//-858  -36  -1385