﻿using UnityEngine;
using System.Collections;

public class NodeInformation : MonoBehaviour {
	public GameObject[] NextNode;
	public bool IsGo = true;
	public Light Red = null;
	public Light Blue = null;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Blue == null)
			return;
		if (Blue.enabled) {
			IsGo = true;
			return;
		}
		IsGo = false;
		/*
		if (Red == null) {
			return;
		}
		if (Red.enabled) {
			IsGo = true;
			return;
		}
		IsGo = false;
		*/
	}
}
